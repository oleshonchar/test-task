from sqlalchemy import create_engine, Column, Integer, Text, Float, String, BigInteger, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import datetime

from config import DB_LOGIN, DB_PASSWORD


engine = create_engine('postgresql://{}:{}@localhost:5432/database'.format(DB_LOGIN, DB_PASSWORD))
session = sessionmaker(bind=engine)
base = declarative_base()


class Product(base):
    __tablename__ = 'products'
    id = Column(Integer, primary_key=True)
    article = Column('article', BigInteger)
    title = Column('title', Text)
    price = Column('price', Float)
    gross = Column('gross', Float)
    sale_price = Column('sale_price', Integer)
    category = Column('category', String(32))
    created = Column('created', DateTime, default=datetime.datetime.now)
    updated = Column('updated', DateTime, onupdate=datetime.datetime.now)
