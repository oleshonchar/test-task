from collections import OrderedDict
import os

from config import REP_BY_CAT_NAME, REP_BY_PR_DIFF_NAME


def get_json_by_category(db_session, Product):
    """Creates json report

        Note:
            The function makes one request to the
            database and generates a report. The report
            is divided into days that include the
            count of products by category

        Args:
            db_session (sqlalchemy.orm.session.Session):
            open database session
            Product (sqlalchemy.ext.declarative.api.DeclarativeMeta):
            model in the database

        Returns:
            True and sorted report if successful,
            False and error message otherwise
    """
    products = db_session.query(Product).all()
    if not products:
        return False, 'Database is empty'
    json_categories_report = {}
    for product in products:
        if str(product.created.date()) not in json_categories_report:
            json_categories_report[str(product.created.date())] = {}
        if 'category' not in json_categories_report[str(product.created.date())]:
            json_categories_report[str(product.created.date())]['category'] = {}
        if product.category not in json_categories_report[str(product.created.date())]['category']:
            json_categories_report[str(product.created.date())]['category'][product.category] = []
        json_categories_report[str(product.created.date())]['category'][product.category].append(product.title)
    return True, OrderedDict(sorted(json_categories_report.items()))


def get_report_by_category(db_session, Product, ET):
    """Creates xml report and saves it

        Note:
            The function receives json with data about
            products and generates xml report
            that is saved in the "reports" folder

        Args:
            db_session (sqlalchemy.orm.session.Session):
            open database session
            Product (sqlalchemy.ext.declarative.api.DeclarativeMeta):
            model in the database
            ET (module): tool for work with xml files

        Returns:
            True and message if successful,
            False and error message otherwise
    """
    status, data = get_json_by_category(db_session, Product)
    if not status:
        return False, 'Database is empty'
    root = ET.Element('root')
    for day in data:
        document = ET.SubElement(root, 'day', name=day)
        for category in data[day]['category']:
            ET.SubElement(document, 'category', name=category).text = str(len(data[day]['category'][category]))
    tree = ET.ElementTree(root)
    try:
        tree.write('reports/' + REP_BY_CAT_NAME)
    except FileNotFoundError:
        os.makedirs('reports')
        tree.write('reports/' + REP_BY_CAT_NAME)
    return True, 'Report by category was created'


def get_report_by_price_difference(db_session, Product, ET):
    """Creates xml report and saves it

        Note:
            The function receive data from the database,
            filters according to a given condition,
            generates xml report,
            and saves it in the "reports" folder

        Args:
            db_session (sqlalchemy.orm.session.Session):
            open database session
            Product (sqlalchemy.ext.declarative.api.DeclarativeMeta):
            model in the database
            ET (module): tool for work with xml files

        Returns:
            True and message if successful,
            False and error message otherwise
    """
    products = db_session.query(Product).order_by(Product.price)
    if not products.count():
        return False, 'Database is empty'
    root = ET.Element('root')
    document = ET.SubElement(root, 'products')
    for product in products:
        func = (product.sale_price * 100) / product.price
        if 95 <= func <= 105:
            ET.SubElement(document, 'item').text = product.title
    tree = ET.ElementTree(root)
    try:
        tree.write('reports/' + REP_BY_PR_DIFF_NAME)
    except FileNotFoundError:
        os.makedirs('reports')
        tree.write('reports/' + REP_BY_PR_DIFF_NAME)
    return True, 'Report by price difference was created'
