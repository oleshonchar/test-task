import requests
import xml.etree.ElementTree as ET
import csv
from ftplib import FTP, error_perm
import os
import sys

from model import session, base, engine, Product
from config import URL, NAMESPACE, ADDITIONAL_INFO, PRODUCT_ID_LIST, HOST, FTP_LOG, FTP_PWD, PORT, ITEM_ID, \
    REP_BY_CAT_NAME, REP_BY_PR_DIFF_NAME
from reporting import get_report_by_category, get_report_by_price_difference


base.metadata.create_all(engine)
db_session = session()


def get_product_id_list(file='sources/product-id-list.csv'):
    """Imports data from csv file

        Note:
            The function retrieves product ids from file

        Args:
            file (str): path to file,
            default path 'sources/product-id-list.csv'

        Returns:
            True and list with products id if successful,
            False and error message otherwise
    """
    filename, file_extension = os.path.splitext(file)
    if file_extension != '.csv':
        return False, 'Incorrect file extension'
    product_id_list = set()
    try:
        with open(file, newline='') as f:
            reader = csv.reader(f)
            for row in reader:
                product_id_list.add(row[0])
    except FileNotFoundError as e:
        print(e)
        return False, 'File not found'
    if not product_id_list:
        return False, 'File is empty'
    return True, product_id_list


def get_additional_info(file='sources/additional-info.csv'):
    """Imports data from csv file

        Note:
            The function retrieves additional
            information about product

        Args:
            file (str): path to file,
            default path 'sources/additional-info.csv'

        Returns:
            True and list of product additional info if successful,
            False and error message otherwise
    """
    filename, file_extension = os.path.splitext(file)
    if file_extension != '.csv':
        return False, 'Incorrect file extension'
    additional_info = {}
    try:
        with open(file, newline='') as f:
            reader = csv.DictReader(f)
            for row in reader:
                try:
                    additional_info[row['Артикул']] = {
                        'gross': row['Вес в упаковке, г'],
                        'color': row['Цвет'],
                        'promotion': row['Акция'],
                    }
                except KeyError as e:
                    return False, 'Incorrect header {} in file'.format(e)
    except FileNotFoundError as e:
        print(e)
        return False, 'File not found'
    if not additional_info:
        return False, 'File is empty'
    return True, additional_info


def get_filtered_products(root, product_id_list):
    """Filters products

        Note:
            The function reads the xml file and
            filters elements

        Args:
            root (xml.etree.ElementTree.Element):
            root element on xml file
            product_id_list (set): list of product id

        Returns:
            True and list of filtered products if successful,
            False and error message otherwise
    """
    products_list = []
    if not product_id_list:
        return False, 'Id list is empty'
    for item in root.iter('item'):
        try:
            article = item.find(NAMESPACE + ITEM_ID).text
        except AttributeError:
            return False, 'Item not found'
        new_article = article if article[0] != '0' else article[1:]
        if new_article in product_id_list:
            item.find(NAMESPACE + ITEM_ID).text = str(new_article)
            products_list.append(item)
    return True, products_list


def add_new_element(item, el, title):
    child = ET.Element(NAMESPACE + title)
    child.text = el
    item.append(child)


def upgrade_product(additional_info, products_list):
    """Upgrades available products

        Note:
            The function adds new fields for products,
            new fields are extracted from the csv file

        Args:
            additional_info (): list of product additional info
            products_list (dict): list of products in xml format

        Returns:
            True and message if successful,
            False and error message otherwise
    """
    if not products_list:
        return False, 'Id list is empty'
    if not additional_info:
        return False, 'File with additional info is empty'
    for item in products_list:
        gross = additional_info[item.find(NAMESPACE + ITEM_ID).text]['gross']
        color = additional_info[item.find(NAMESPACE + ITEM_ID).text]['color']
        promotion = additional_info[item.find(NAMESPACE + ITEM_ID).text]['promotion']
        add_new_element(item, gross, 'gross')
        add_new_element(item, color, 'color')
        add_new_element(item, promotion, 'promotion')
    return True, 'Done, products list was upgrade'


def save_product(products_list):
    """Saves items to database

        Args:
            products_list (dict): list of products in xml format

        Returns:
            True and message if successful,
            False and error message otherwise
    """
    for item in products_list:
        product = Product()
        try:
            product.article = item.find('{}gtin'.format(NAMESPACE)).text
            product.title = item.find('title').text
            product.price = item.find('{}price'.format(NAMESPACE)).text
            product.gross = item.find('{}gross'.format(NAMESPACE)).text
            product.sale_price = item.find('{}sale-price'.format(NAMESPACE)).text
            product.category = item.find('{}product_type'.format(NAMESPACE)).text
        except AttributeError as e:
            print(e)
            return False, 'Incorrect format for saving'
        db_session.add(product)
    return True, 'Done, products were saved'


def upload_report(path, name):
    """Sends report to server

        Note:
            The function connects to server
            and uploads report in xml format

        Args:
            path (str): path to file
            name (str): report name

        Returns:
            True and message if successful,
            False and error message otherwise
    """
    with FTP() as ftp:
        ftp.connect(host=HOST, port=PORT)
        try:
            ftp.login(user=FTP_LOG, passwd=FTP_PWD)
        except error_perm:
            return False, 'Failed to login'
        try:
            with open(path, 'rb') as file:
                ftp.storbinary('STOR ' + name, file)
        except FileNotFoundError:
            return False, 'Reports not found!'
        return True, 'Report was sent'


def main():

    print('\nAttention! Before you start, you must fill in the configuration file!\n')
    command = input('Did you fill your configuration file? (y/n)\n')
    if command.upper() == 'Y':
        while True:
            print('\n[start] make a selection products\n[create] make reports\n[send] send reports\n'
                  '[exit] exit\n')
            command = input('Enter the command:\n')
            if command.upper() == 'START':
                product_id_list = get_product_id_list(file=PRODUCT_ID_LIST)[1]
                additional_info = get_additional_info(file=ADDITIONAL_INFO)[1]
                response = requests.get(URL, stream=True)
                response.raw.decode_content = True
                tree = ET.parse(response.raw)
                root = tree.getroot()
                if isinstance(product_id_list, set):
                    status, products_list = get_filtered_products(root, product_id_list)
                else:
                    return False, 'Type error'
                if products_list and additional_info:
                    status, message = upgrade_product(additional_info, products_list)
                    print('\n' + message)
                    status, message = save_product(products_list)
                    print(message)
                    db_session.commit()
                    db_session.close()
            elif command.upper() == 'CREATE':
                status, message = get_report_by_category(db_session, Product, ET)
                if status is True:
                    print('\n' + message)
                status, message = get_report_by_price_difference(db_session, Product, ET)
                if status is True:
                    print(message)
                else:
                    print('\nReports not created')
                    print('Run the command "start"')
            elif command.upper() == 'SEND':
                get_report_by_category(db_session, Product, ET)
                get_report_by_price_difference(db_session, Product, ET)
                status, message = upload_report('reports/' + REP_BY_CAT_NAME, REP_BY_CAT_NAME)
                if status is True:
                    print('\n' + message)
                status, message = upload_report('reports/' + REP_BY_PR_DIFF_NAME, REP_BY_PR_DIFF_NAME)
                if status is True:
                    print(message)
                else:
                    print('\nReports is not sent')
                    print('Run the command "start"')
            elif command.upper() == 'EXIT':
                print('\nGood bye!')
                sys.exit()
            else:
                print('\nThis command does not exist!')
    else:
        print('\nFill in the configuration file and run the program!')
        sys.exit()


if __name__ == "__main__":
    main()
