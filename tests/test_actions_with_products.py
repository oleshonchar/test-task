import pytest
import requests
import xml.etree.ElementTree as ET

from script import get_filtered_products, upgrade_product, get_additional_info, save_product, db_session, Product
from config import URL, NAMESPACE, ADDITIONAL_INFO


class TestFilteredProducts:
    def setup_method(self):
        response = requests.get(URL, stream=True)
        response.raw.decode_content = True
        tree = ET.parse(response.raw)
        self.root = tree.getroot()

    def test_get_filtered_products_emtpy_list(self):
        product_id_list = {}
        status, message = get_filtered_products(self.root, product_id_list)
        assert status is False
        assert message in 'Id list is empty'

    def test_get_filtered_products_correct_list(self):
        product_id_list = {'3474635001775', '9339341002284', '884486095190'}
        status, data = get_filtered_products(self.root, product_id_list)
        assert isinstance(data, list)
        for item in data:
            assert isinstance(item, ET.Element)

    def test_get_filtered_products_zero_detection(self):
        item_id = 'gtin'
        product_id_list = {'3474635001775', '9339341002284', '884486095190'}
        status, data = get_filtered_products(self.root, product_id_list)
        for item in data:
            assert item.find(NAMESPACE + item_id).text[:1] != '0'

    def test_upgrade_product_empty_additional_info(self):
        additional_info = {}
        product_id_list = {'3474635001775', '9339341002284', '884486095190'}
        status, products_list = get_filtered_products(self.root, product_id_list)
        status, message = upgrade_product(additional_info, products_list)
        assert status is False
        assert message in 'File with additional info is empty'

    def test_upgrade_product_empty_product_id_list(self):
        status, additional_info = get_additional_info(file=ADDITIONAL_INFO)
        products_list = []
        status, message = upgrade_product(additional_info, products_list)
        assert status is False
        assert message in 'Id list is empty'

    def test_upgrade_product_correct_data(self):
        status, additional_info = get_additional_info(file=ADDITIONAL_INFO)
        product_id_list = {'3474635001775', '9339341002284', '884486095190'}
        status, products_list = get_filtered_products(self.root, product_id_list)
        status, message = upgrade_product(additional_info, products_list)
        assert status is True
        assert message in 'Done, products list was upgrade'

    def test_upgrade_product_add_new_el(self):
        status, additional_info = get_additional_info(file=ADDITIONAL_INFO)
        status, products_list = get_filtered_products(self.root, {'3474635001775'})
        with pytest.raises(AttributeError):
            products_list[0].find('{}color'.format(NAMESPACE)).text
        upgrade_product(additional_info, products_list)
        assert products_list[0].find('{}color'.format(NAMESPACE)).text == 'Синий'

    def test_save_product_correct_data_without_required_field(self):
        status, products_list = get_filtered_products(self.root, {'3474635001775'})
        status, message = save_product(products_list)
        assert status is False
        assert message in 'Incorrect format for saving'

    def test_save_product_correct_saving(self):
        status, additional_info = get_additional_info(file=ADDITIONAL_INFO)
        status, products_list = get_filtered_products(self.root, {'3474635001775'})
        upgrade_product(additional_info, products_list)
        products_list[0].find('title').text = 'TEST'
        result = db_session.query(Product).filter(Product.title == 'TEST').count()
        assert result == 0
        status, message = save_product(products_list)
        db_session.commit()
        result = db_session.query(Product).filter(Product.title == 'TEST').count()
        assert result == 1
        db_session.query(Product).filter(Product.title == 'TEST').delete()
        db_session.commit()
        assert status is True
        assert message in 'Done, products were saved'
