import pytest
import csv
import os
import xml.etree.ElementTree as ET

from script import get_product_id_list, get_additional_info


class TestImportCsv:
    def test_get_product_id_list_file_not_found(self):
        status, message = get_product_id_list(file='some_file.csv')
        assert status is False
        assert message in 'File not found'

    def test_get_product_id_listemtpy_file(self):
        with open('some_file.csv', 'w', newline='') as f:
            csv.writer(f)
        status, message = get_product_id_list(file='some_file.csv')
        assert status is False
        assert message in 'File is empty'
        os.remove('some_file.csv')

    def test_get_product_id_list_incorrect_file_extension(self):
        root = ET.Element('root')
        ET.ElementTree(root).write('some_file.xml')
        status, message = get_product_id_list(file='some_file.xml')
        assert status is False
        assert message in 'Incorrect file extension'
        os.remove('some_file.xml')

    def test_get_product_id_list_correct_file(self):
        with open('some_file.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow('1')
            writer.writerow('2')
            writer.writerow('3')
        status, data = get_product_id_list(file='some_file.csv')
        assert data == {'1', '2', '3'}
        os.remove('some_file.csv')

    def test_get_product_id_list_filter_not_unique_values(self):
        with open('some_file.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow('1')
            writer.writerow('1')
            writer.writerow('1')
        status, data = get_product_id_list(file='some_file.csv')
        assert data == {'1'}
        os.remove('some_file.csv')

    def test_get_additional_info_file_not_found(self):
        status, message = get_additional_info(file='some_file.csv')
        assert status is False
        assert message in 'File not found'

    def test_get_additional_info_emtpy_file(self):
        with open('some_file.csv', 'w', newline='') as f:
            csv.writer(f)
        status, message = get_additional_info(file='some_file.csv')
        assert status is False
        assert message in 'File is empty'
        os.remove('some_file.csv')

    def test_get_additional_info_incorrect_file_extension(self):
        root = ET.Element('root')
        ET.ElementTree(root).write('some_file.xml')
        status, message = get_additional_info(file='some_file.xml')
        assert status is False
        assert message in 'Incorrect file extension'
        os.remove('some_file.xml')

    def test_get_additional_info_correct_file(self):
        with open('some_file.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['Артикул', 'Вес в упаковке, г', 'Цвет', 'Акция'])
            writer.writerow(['9339341002284', '128', 'Желтый', '-'])
        status, data = get_additional_info(file='some_file.csv')
        assert data == {'9339341002284': {'promotion': '-', 'gross': '128', 'color': 'Желтый'}}
        os.remove('some_file.csv')

    def test_get_additional_info_incorrect_headers(self):
        with open('some_file.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['Артикуль', 'Вес в упаковке, г', 'Цвет', 'Акция'])
            writer.writerow(['9339341002284', '128', 'Желтый', '-'])
        status, message = get_additional_info(file='some_file.csv')
        assert status is False
        assert message in "Incorrect header 'Артикул' in file"
        os.remove('some_file.csv')
